# Without Docker

### `npm install`

### `npm start`

Runs the app\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

# With Docker

### `docker build -t eth-container:dev .`

### `docker run -it --rm -v ${PWD}:/app -v /app/node_modules -p 3001:3000 -e CHOKIDAR_USEPOLLING=true eth-container:dev`

Runs the app\
Open [http://localhost:3001](http://localhost:3001) to view it in the browser.