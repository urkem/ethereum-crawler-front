import React from 'react'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { DataGrid } from '@material-ui/data-grid';
import getTranstactions from './service/getTransactions'

const useStyles = makeStyles((theme) => ({
    submitButton: {
        marginTop: '1ch'
    },
}));

const columns = [
    { field: 'blockNumber', headerName: 'Block', width: 100 },
    { field: 'hash', headerName: 'Txn Hash', width: 200 },
    { field: 'from', headerName: 'From', width: 400 },
    { field: 'to', headerName: 'To', width: 400 },
    {
      field: 'value',
      headerName: 'Value',
      type: 'number',
      width: 160,
      valueFormatter: ({ value }) => (value / 10**18),
    },
  ];

function Transactions() {
    const classes = useStyles();

    const [transactions, setTransactions] = React.useState([]);
    const [wallet, setWallet] = React.useState("");
    const [block, setBlock] = React.useState("");

    const handleSubmit = (event) => {
        event.preventDefault();
        getTranstactions(wallet,block)
        .then(
            (data) => {
                data.result = data.result.map(x=>{
                    x.id = x.blockNumber
                    return x
                })
                setTransactions(data.result)
            }
        )
    };


    return (
    <form noValidate autoComplete="off" onSubmit={handleSubmit}>
        <Grid container spacing={5}>
            <Grid item xs={12} lg={7} sm={6}>
            <TextField 
                id="wallet-address"
                label="Wallet Address"
                fullWidth
                onInput={ e=>setWallet(e.target.value)}
            />
            </Grid>
            <Grid item xs={12} lg={3} sm={3}>
            <TextField                
                id="block-number"
                label="Block Number"
                fullWidth
                onInput={ e=>setBlock(e.target.value)}
            />
            </Grid>
            <Grid item xs={12} lg={2} sm={3}>
            <Button
                variant="contained" 
                color="primary"
                className={classes.submitButton}
                type="submit">
                Get Transactions
            </Button>
            </Grid>
            <Grid xs={12} style={{ height: '80vh'}}>
                <DataGrid rows={transactions} columns={columns} pageSize={10}/>
            </Grid>
        </Grid>
    </form>
    );
}

export default Transactions;