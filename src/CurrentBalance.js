import React from 'react';
import getCurrentBalance from './service/getCurrentBalance';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles((theme) => ({
    submitButton: {
        marginTop: '1ch'
    },
}));

export default function CurrentBalance() {
    const classes = useStyles();

    const [wallet, setWallet] = React.useState("");
    const [balance, setbalance] = React.useState("");

    const handleSubmit = (event) => {
        event.preventDefault();
        getCurrentBalance(wallet)
        .then(
            (data) => {
                setbalance(data.result / 10**18)
            }
        )
    };
  
  
    return (
    <form noValidate autoComplete="off" onSubmit={handleSubmit}>
        <Grid container spacing={5}>
            <Grid item xs={12} lg={7} sm={6}>
            <TextField 
                id="wallet-address"
                label="Wallet Address"
                fullWidth
                onInput={ e=>setWallet(e.target.value)}
            />
            </Grid>
            <Grid item xs={12} lg={3} sm={4}>
            <Button
                variant="contained" 
                color="primary"
                className={classes.submitButton}
                type="submit">
                Get Balance
            </Button>
            </Grid>
            <Grid item xs={12} style={{textAlign: 'center'}}>
                {balance !== "" && (
                    <Typography variant="h4" >Balance: {balance} Ether</Typography>
                )}
            </Grid>
        </Grid>
    </form>
    );
  }